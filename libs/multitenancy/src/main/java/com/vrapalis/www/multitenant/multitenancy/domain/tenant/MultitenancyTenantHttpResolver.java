package com.vrapalis.www.multitenant.multitenancy.domain.tenant;

import org.springframework.lang.NonNull;
import jakarta.servlet.http.HttpServletRequest;

@FunctionalInterface
public interface MultitenancyTenantHttpResolver<T extends HttpServletRequest> {

    String resolveTenantId(@NonNull T httpServletRequest);
}
