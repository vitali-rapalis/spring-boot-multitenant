package com.vrapalis.www.multitenant.multitenancy.config;

import com.vrapalis.www.multitenant.multitenancy.domain.tenant.MultitenancyTenantFilter;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManagerResolver;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.server.resource.web.authentication.BearerTokenAuthenticationFilter;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
public class MultitenancySecurityConfiguration {

    @Bean
    SecurityFilterChain securityFilterChain(
            HttpSecurity http, AuthenticationManagerResolver<HttpServletRequest> authManagerResolver,
            MultitenancyTenantFilter tenantFilter
	) throws Exception {
		return http
			.authorizeHttpRequests(request -> request
				.requestMatchers("/actuator/**").permitAll()
				.anyRequest().authenticated())
			.oauth2ResourceServer(oauth2 -> oauth2.authenticationManagerResolver(authManagerResolver))
			.addFilterBefore(tenantFilter, BearerTokenAuthenticationFilter.class)
			.build();
	}
}
