package com.vrapalis.www.multitenant.multitenancy.config;

import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.ComponentScan;

@AutoConfiguration
@ComponentScan(basePackages = "com.vrapalis.www.multitenant.multitenancy")
@ConfigurationPropertiesScan(basePackages = "com.vrapalis.www.multitenant.multitenancy")
public class MultitenancyAutoConfiguration {
}
