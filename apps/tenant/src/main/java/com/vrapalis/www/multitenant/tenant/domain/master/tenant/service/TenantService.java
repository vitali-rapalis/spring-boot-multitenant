package com.vrapalis.www.multitenant.tenant.domain.master.tenant.service;

import jakarta.annotation.PostConstruct;
import jakarta.persistence.EntityNotFoundException;
import liquibase.exception.LiquibaseException;
import liquibase.integration.spring.SpringLiquibase;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.core.io.ResourceLoader;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.StatementCallback;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.vrapalis.www.multitenant.tenant.domain.master.tenant.repository.TenantRepository;

import javax.sql.DataSource;

@Slf4j
@Service
@Transactional
public class TenantService implements ITenantService {

    private final TenantRepository tenantRepository;
    private final JdbcTemplate jdbcTemplate;
    private final DataSource dataSource;
    private final LiquibaseProperties liquibaseProperties;
    private final ResourceLoader resourceLoader;
    private ThreadLocal<String> tenantLocal = new ThreadLocal<>();

    public TenantService(TenantRepository tenantRepository, JdbcTemplate jdbcTemplate, DataSource dataSource,
                         @Qualifier("tenantsLiquibaseProperties") LiquibaseProperties liquibaseProperties, ResourceLoader resourceLoader) {
        this.tenantRepository = tenantRepository;
        this.jdbcTemplate = jdbcTemplate;
        this.dataSource = dataSource;
        this.liquibaseProperties = liquibaseProperties;
        this.resourceLoader = resourceLoader;
    }

    @PostConstruct
    public void init() {
        tenantLocal.set("public");
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.REQUIRES_NEW)
    public boolean setTenant(String tenantName) {
        val tenant = tenantRepository.findBySchemaName(tenantName).orElseThrow(EntityNotFoundException::new);
        tenantLocal.set(tenant.getSchemaName());
        return true;
    }

    public void createTenant(String schema) {
        try {
            jdbcTemplate.execute((StatementCallback<Boolean>) stmt -> stmt.execute("CREATE SCHEMA " + schema));
            SpringLiquibase springLiquibase = getSpringLiquibase(dataSource, schema);
            springLiquibase.afterPropertiesSet();
            log.debug("Liquibase migration scripts run for tenant: {}", schema);
        } catch (LiquibaseException e) {
            log.error("");
        }
    }

    private SpringLiquibase getSpringLiquibase(DataSource dataSource, String schema) {
        SpringLiquibase liquibase = new SpringLiquibase();
        liquibase.setResourceLoader(resourceLoader);
        liquibase.setDataSource(dataSource);
        liquibase.setDefaultSchema(schema);
        liquibase.setChangeLog(liquibaseProperties.getChangeLog());
        liquibase.setContexts(liquibaseProperties.getContexts());
        return liquibase;
    }
}
