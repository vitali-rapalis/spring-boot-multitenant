package com.vrapalis.www.multitenant.tenant.domain.tenants.car.dto;

import lombok.Value;

import java.util.UUID;

@Value
public class CarDto {
    private UUID id;
    private String name;
}
