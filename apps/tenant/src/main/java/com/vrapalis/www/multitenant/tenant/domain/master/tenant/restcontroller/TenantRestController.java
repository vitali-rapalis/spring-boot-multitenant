package com.vrapalis.www.multitenant.tenant.domain.master.tenant.restcontroller;

import com.vrapalis.www.multitenant.tenant.domain.master.tenant.service.TenantService;
import liquibase.exception.LiquibaseException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/tenants")
public class TenantRestController {

    private final TenantService tenantService;

    @PostMapping
    public ResponseEntity<Void> createTenant(@RequestParam String schema) throws LiquibaseException {
        this.tenantService.createTenant(schema);
        return ResponseEntity.ok().build();
    }
}

