package com.vrapalis.www.multitenant.tenant.domain.master.tenant.interceptor;

import com.vrapalis.www.multitenant.tenant.domain.master.tenant.service.ITenantContextService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import com.vrapalis.www.multitenant.tenant.domain.master.tenant.resolver.ITenantResolver;

@Slf4j
@Component
@RequiredArgsConstructor
public class TenantInterceptor implements HandlerInterceptor {

    private final ITenantResolver tenantResolver;
    private final ITenantContextService tContextService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        val tenantOptional = tenantResolver.resolveTenantName(request);
        return tenantOptional.map(tenantName -> tContextService.setTenant(tenantName)).orElse(false);
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        tContextService.clearTenant();
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        tContextService.clearTenant();
    }
}
