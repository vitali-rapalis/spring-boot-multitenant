package com.vrapalis.www.multitenant.tenant.config.jpa;


import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class JpaCustomProperties {
    private JpaPackageProperty repositories;
    private JpaPackageProperty entities;
}

@Data
@NoArgsConstructor
class JpaPackageProperty {
    private String packages;
}