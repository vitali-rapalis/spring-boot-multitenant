package com.vrapalis.www.multitenant.tenant.domain.master.tenant.entity;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Entity
@Table(name = "tenant", schema = "public")
@Data
@NoArgsConstructor
public class TenantEntity implements Serializable {

    @Id
    @Column(name = "schema_name", length = 30, unique = true, nullable = false)
    private String schemaName;

    @Column(length = 360, nullable = false)
    private String description;

}
