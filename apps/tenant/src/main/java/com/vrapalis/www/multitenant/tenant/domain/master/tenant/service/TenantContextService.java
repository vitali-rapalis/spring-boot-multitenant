package com.vrapalis.www.multitenant.tenant.domain.master.tenant.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class TenantContextService implements ITenantContextService {

    @Override
    public boolean setTenant(String tenantName) {
        TENANT_NAME.set(tenantName);
        log.debug("Tenant name set to: {}", tenantName);
        return true;
    }

    @Override
    public boolean clearTenant() {
        TENANT_NAME.remove();
        return true;
    }

    @Override
    public String getCurrentTenant() {
        return TENANT_NAME.get();
    }
}
