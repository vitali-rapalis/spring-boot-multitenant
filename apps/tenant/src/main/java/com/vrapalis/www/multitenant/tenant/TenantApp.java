package com.vrapalis.www.multitenant.tenant;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TenantApp {

    public static void main(String[] args) {
        SpringApplication.run(TenantApp.class, args);
    }
}