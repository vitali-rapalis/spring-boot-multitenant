package com.vrapalis.www.multitenant.tenant.config.db.migration;

import com.vrapalis.www.multitenant.tenant.domain.master.tenant.repository.TenantRepository;
import com.vrapalis.www.multitenant.tenant.domain.master.tenant.service.TenantService;
import jakarta.annotation.PostConstruct;
import liquibase.exception.LiquibaseException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.DataAccessException;
import com.vrapalis.www.multitenant.tenant.domain.master.tenant.entity.TenantEntity;


@Slf4j
@Configuration
@ConditionalOnProperty(name = "multi-tenancy.tenants.liquibase.enabled", havingValue = "true", matchIfMissing = true)
public class LiquibaseTenantsConfiguration {

    private final TenantRepository tenantRepository;
    private final TenantService tenantService;

    public LiquibaseTenantsConfiguration(TenantRepository tenantRepository, TenantService tenantService) {
        this.tenantRepository = tenantRepository;
        this.tenantService = tenantService;
    }

    @PostConstruct
    public void init() throws LiquibaseException {
        for (TenantEntity tenantEntity : tenantRepository.findAll()) {
            try {
                tenantService.createTenant(tenantEntity.getSchemaName());
            } catch (DataAccessException e) {
                log.error("Liquibase can't run for tenant: {}", tenantEntity.getSchemaName());
            }
        }
    }
}
