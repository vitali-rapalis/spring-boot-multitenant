package com.vrapalis.www.multitenant.tenant.domain.master.tenant.property;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@NoArgsConstructor
@ConfigurationProperties("multi-tenancy")
public class TenantProperty {
    private String tenantHeader;
}
