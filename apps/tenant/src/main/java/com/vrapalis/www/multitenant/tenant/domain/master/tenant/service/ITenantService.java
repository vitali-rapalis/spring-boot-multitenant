package com.vrapalis.www.multitenant.tenant.domain.master.tenant.service;

public interface ITenantService {
    boolean setTenant(String tenantName);
}
