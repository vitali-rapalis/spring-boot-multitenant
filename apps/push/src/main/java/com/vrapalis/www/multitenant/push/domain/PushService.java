package com.vrapalis.www.multitenant.push.domain;

public interface PushService {

    String sendEmail();
}
