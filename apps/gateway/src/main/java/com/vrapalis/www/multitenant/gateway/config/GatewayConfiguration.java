package com.vrapalis.www.multitenant.gateway.config;

import com.vrapalis.www.multitenant.gateway.domain.security.GatewayTenantSecurityProperties;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationPropertiesScan(basePackageClasses = GatewayTenantSecurityProperties.class)
public class GatewayConfiguration {

}
