package com.vrapalis.www.multitenant.gateway.config;

import com.vrapalis.www.multitenant.gateway.domain.client.GatewayClientRegistrationRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.security.web.server.authentication.RedirectServerAuthenticationEntryPoint;

@Configuration
//@EnableWebFluxSecurity
public class GatewaySecurityConfiguration {

    @Bean
    SecurityWebFilterChain springSecurityFilterChain(ServerHttpSecurity http, GatewayClientRegistrationRepository clientRegistrationRepository) {
		return http
			.authorizeExchange(exchange -> exchange
				.pathMatchers("/actuator/**", "/tenant-login/**").permitAll()
				.anyExchange().authenticated())
			.oauth2Login(oauth2 -> oauth2
				.clientRegistrationRepository(clientRegistrationRepository))
			.exceptionHandling(exception ->
				exception.authenticationEntryPoint(new RedirectServerAuthenticationEntryPoint("/tenant-login")))
			.build();
	}
}
