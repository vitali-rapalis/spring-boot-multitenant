package com.vrapalis.www.multitenant.gateway.domain.client;

import com.vrapalis.www.multitenant.gateway.domain.security.GatewayTenantSecurityProperties;
import lombok.RequiredArgsConstructor;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrations;
import org.springframework.security.oauth2.client.registration.ReactiveClientRegistrationRepository;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
@RequiredArgsConstructor
public class GatewayClientRegistrationRepository implements ReactiveClientRegistrationRepository {

    private static final Map<String, Mono<ClientRegistration>> clientRegistrations = new ConcurrentHashMap<>();
	private final GatewayTenantSecurityProperties tenantSecurityProperties;

	@Override
	public Mono<ClientRegistration> findByRegistrationId(String registrationId) {
		return clientRegistrations.computeIfAbsent(registrationId, this::buildClientRegistration);
	}

	private Mono<ClientRegistration> buildClientRegistration(String registrationId) {
		return Mono.just(ClientRegistrations.fromOidcIssuerLocation(computeTenantIssuerUri(registrationId))
			.registrationId(registrationId)
			.clientId(tenantSecurityProperties.clientId())
			.clientSecret(tenantSecurityProperties.clientSecret())
			.redirectUri("{baseUrl}/login/oauth2/code/" + registrationId)
			.scope("openid")
			.build());
	}

	private String computeTenantIssuerUri(String tenantId) {
		var issuerBaseUri = tenantSecurityProperties.issuerBaseUri().toString().strip();
		return issuerBaseUri + tenantId;
	}

}
